<?php

class datasource {
	
	var $query = "";
	var $db = "";
	
	var	$field_names = array();

	 var $glob = array('driver' => 'mysql',
						 'connect' => 'mysqli_connect',
						 'host' => 'localhost',
						 'login' => 'root',
						 'password' => "",
						 'database' => 'corpkeytest',
						 'prefix' => ''); 	   
						  
	  
	function db()
	{
		
		$this->db = mysqli_connect($this->glob['host'], $this->glob['login'], $this->glob['password'], $this->glob['database']);	
		if (!$this->db) die ($this->debug(true));	
		
		$selectdb = mysqli_select_db($this->db, $this->glob['database']);
		if (!$selectdb) die ($this->debug());
	
	} // end constructor
	
	function selectQuery($table, $condition){
		$where = ($condition!="")? "WHERE ".$condition : "";
		$qey_table = "SELECT * FROM $table ".$where;
		return mysqli_query($qey_table);
	}
	
	
	function table_exists ($table) { 
	$tables = mysqli_list_tables ($this->glob['database']); 
	while (list ($temp) = mysqli_fetch_array ($tables)) {
		if ($temp == $table) {
			return TRUE;
		}
	}
	return FALSE;
 }

	function getFields($table) {

	  $result = $this->selectQuery($table, $condition);

		for($i=0;$i<mysqli_num_rows($result);$i++){
		   $row = mysqli_fetch_array($result);
		   $name = $row['address'];
		   array_push($field_names, $name);
		   echo $field_names;
		  }
		 
		//return $field_names;
	}
	


	function select($query, $maxRows=0, $pageNum=0)
	{
		$this->query = $query;
		
		// start limit if $maxRows is greater than 0
		if($maxRows>0)
		{
			$startRow = $pageNum * $maxRows;
			$query = sprintf("%s LIMIT %d, %d", $query, $startRow, $maxRows);
		}	
		
		$result = mysqli_query( $this->db,$query);
		
		if ($this->error()) die ($this->debug());
		
		$output=false;
		
		for ($n=0; $n < mysqli_num_rows($result); $n++)
		{
			$row = mysqli_fetch_assoc($result);
			$output[$n] = $row;
		}
	
		return $output;
		
	} // end select
	
	function misc($query) {
	
		$this->query = $query;
		$result = mysqli_query( $this->db,$query);
		
		if ($this->error()) die ($this->debug());
		
		if($result == TRUE){
		
			return TRUE;
			
		} else {
		
			return FALSE;
			
		}
		
	}
	
	function numrows($query) {
		$this->query = $query;
		$result = mysqli_query( $this->db,$query);
		return mysqli_num_rows($result);
	}
	
	
	function insert ($tablename, $record)
	{
		if(!is_array($record)) die ($this->debug("array", "Insert", $tablename));
		
		$count = 0;
		foreach ($record as $key => $val)
		{
			if ($count==0) {$fields = "`".$key."`"; $values = $val;}
			else {$fields .= ", "."`".$key."`"; $values .= ", ".$val;}
			$count++;
		}	
		
		$query = "INSERT INTO ".$tablename." (".$fields.") VALUES (".$values.")";
		
		$this->query = $query;
		mysqli_query( $this->db,$query);
		
		if ($this->error()) die ($this->debug());
		
		if ($this->affected() > 0) return true; else return false;
		
	} // end insert
	
	
	function update ($tablename, $record, $where)
	{
		if(!is_array($record)) die ($this->debug("array", "Update", $tablename));
	
		$count = 0;
		
		foreach ($record as $key => $val)
		{
			if ($count==0) $set = "`".$key."`"."=".$val;
			else $set .= ", " . "`".$key."`". "= ".$val;
			$count++;
		}	
	
		$query = "UPDATE ".$tablename." SET ".$set." WHERE ".$where;
		
		$this->query = $query;
		mysqli_query( $this->db,$query);
		if ($this->error()) die ($this->debug());
		
		if ($this->affected() > 0) return true; else return false;
		
	} // end update
	
	
	
	function delete($tablename, $where, $limit="")
	{
		$query = "DELETE from ".$tablename." WHERE ".$where;
		if ($limit!="") $query .= " LIMIT " . $limit;
		$this->query = $query;
		mysqli_query( $this->db,$query);
		
		if ($this->error()) die ($this->debug());
	
		if ($this->affected() > 0){ 
			return TRUE; 
		} else { 
			return FALSE;
		}
	
	} // end delete
	
	//////////////////////////////////
	// Clean SQL Variables (Security Function)
	////////
	function mySQLSafe($value, $quote="'") { 
		
		// strip quotes if already in
		$value = str_replace(array("\'","'"),"&#39;",$value);
		
		// Stripslashes 
		if (get_magic_quotes_gpc()) { 
			$value = stripslashes($value); 
		} 
		// Quote value
		if(version_compare(phpversion(),"4.3.0")=="-1") {
			$value = mysqli_escape_string($value);
		} else {
			$value = mysqli_real_escape_string($this->db, $value);
		}
		$value = $quote . trim($value) . $quote; 
	 
		return $value; 
	}
	
	
	function debug($type="", $action="", $tablename="")
	{
		switch ($type)
		{
			case "connect":
				$message = "MySQL Error Occured";
				$result = mysqli_errno() . ": " . mysqli_error();
				$query = "";
				$output = "Could not connect to the database. Be sure to check that your database connection settings are correct and that the MySQL server in running.";
			break;
		
		
			case "array":
				$message = $action." Error Occured";
				$result = "Could not update ".$tablename." as variable supplied must be an array.";
				$query = "";
				$output = "Sorry an error has occured accessing the database. Be sure to check that your database connection settings are correct and that the MySQL server in running.";
				
			break;
		
			
			default:
				if (mysqli_errno($this->db))
				{
					$message = "MySQL Error Occured";
					$result = mysqli_errno($this->db) . ": " . mysqli_error($this->db);
					$output = "Sorry an error has occured accessing the database. Be sure to check that your database connection settings are correct and that the MySQL server in running.";
				}
				else 
				{
					$message = "MySQL Query Executed Succesfully.";
					$result = mysqli_affected_rows($this->db) . " Rows Affected";
					$output = "view logs for details";
				}
				
				$linebreaks = array("\n", "\r");
				if($this->query != "") $query = "QUERY = " . str_replace($linebreaks, " ", $this->query); else $query = "";
			break;
		}
		
		$output = "<b style='font-family: Arial, Helvetica, sans-serif; color: #0B70CE;'>".$message."</b><br />\n<span style='font-family: Arial, Helvetica, sans-serif; color: #000000;'>".$result."</span><br />\n<p style='Courier New, Courier, mono; border: 1px dashed #666666; padding: 10px; color: #000000;'>".$query."</p>\n";
		
		return $output;
	}
	
	
	function error()
	{
		if (mysqli_errno($this->db)) return true; else return false;
	}
	
	
	function insertid()
	{
		return mysqli_insert_id($this->db);
	}
	
	function affected()
	{
		return mysqli_affected_rows($this->db);
	}
	
	function close() // close conection
	{
		mysqli_close($this->db);
	} 
	
	
}

?>
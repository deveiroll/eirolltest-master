<?php
header('Access-Control-Allow-Origin: *');

if (!defined('SERVER_ROOT')){
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
}
if (!defined('WEB_ROOT')){
	define('WEB_ROOT',SERVER_ROOT."/corptest");
}

include(WEB_ROOT."/php/startup/startup.php");

//this is for edit user
if (isset($_GET['edituser']) and $_GET['edituser']>0){
	session_start();
	$edituser = $_GET['edituser'];
	
	$sql = "SELECT * FROM users WHERE id = $edituser ";
	
	$result = $dblms->select($sql);
	if ($result==false){
		$result =  array('status'=>'failed');
	}
	
	$jreturn = json_encode($result);
	echo $jreturn;
	exit();
}

// this is for new user
if (isset($_POST['action']) && $_POST['action']=="create"){
		
	$login = $_POST['login'];
	
	$checkuser = $dblms->select("SELECT login FROM users WHERE login = '$login' ");
	
	if ( $checkuser==true){
		echo 'x';
	}else{
				
		
		$insertData['firstname'] = $dblms->mySQLSafe($_POST['firstname']);
		$insertData['lastname'] = $dblms->mySQLSafe($_POST['lastname']);
		$insertData['role'] = $dblms->mySQLSafe($_POST['role']);
		$insertData['login'] = $dblms->mySQLSafe($_POST['login']);
		$insertData['email'] = $dblms->mySQLSafe($_POST['email']);
		$insertData['photo'] = $dblms->mySQLSafe($_POST['photo']);
			
			$result1 = $dblms->insert('users',$insertData);
			
		//	$newID = $dblms->insertid();
			
			if($result1 != false){
		//		$result1 = $dblms->select('Select firstname, lastname, login, email, password FROM users WHERE id = $newID');
				
			}else{
				echo 'failed';
			}
	}
};

if (isset($_GET['action']) && $_GET['action']=="delete"){
		
	$id = $_POST['selectedid'];
	
	$result1 = $dblms->delete('users','id='.$dblms->mySQLSafe($id));
	
	if($result1 == true){
		$data["status"] = 'true';
	}else{
		$data["status"] = 'false';	
	}
	
		echo json_encode($data);
	
};


// this is for update 
if (isset($_POST['action']) && $_POST['action']=="update"){
		
	$updateData['firstname'] = $dblms->mySQLSafe($_POST['firstname']);
	$updateData['lastname'] = $dblms->mySQLSafe($_POST['lastname']);
	$updateData['role'] = $dblms->mySQLSafe($_POST['role']);
	$updateData['login'] = $dblms->mySQLSafe($_POST['login']);
	$updateData['email'] = $dblms->mySQLSafe($_POST['email']);
	$updateData['password'] = $dblms->mySQLSafe($_POST['password']);
	$updateData['photo'] = $dblms->mySQLSafe($_POST['photo']);

	$result1 = $dblms->update('users',$updateData,'id='.$dblms->mySQLSafe($_POST['userid']));
			

	if($result1 != false){
		echo 'success';
	}else{
		echo 'failed';
	}
	
};


// this is for listing users
if (isset($_GET['action']) and $_GET['action']=="listing") {
	
	$sql = "SELECT * FROM users";
	$result = $dblms->select($sql);

	if($result ==true){
		for ($i=0;$i < count($result); $i++){
			if ($result[$i]['photo']!=""){
				
				$result[$i]['photo'] = '<img src="/corptest/uploads/'.$result[$i]['photo'].'" width="40px" />';
				
			}else{
				
					$result[$i]['photo'] = '<img src="/corptest/uploads/blank.jpg" width="40px" />';	
				
			}
			//$result[$i]['photo'] = '';
		} 
	}
	
	echo json_encode(array('data'=>$result));
}

// upload photo
if (isset($_GET['action']) and $_GET['action']=="upload") {
	
	//print_r($_FILES);
	$fileName = $_FILES['file']['name'];
	$fileType = $_FILES['file']['type'];
	$fileError = $_FILES['file']['error'];
	$fileContent = file_get_contents($_FILES['file']['tmp_name']);
	 $label = rand(0,50);
	if($fileError == UPLOAD_ERR_OK){
		
		$allowedExts = array("gif", "jpeg", "jpg", "png");
		$temp = explode(".", $_FILES["file"]["name"]);
		$extension = end($temp);
		if ((($_FILES["file"]["type"] == "image/gif")
		|| ($_FILES["file"]["type"] == "image/jpeg")
		|| ($_FILES["file"]["type"] == "image/jpg")
		|| ($_FILES["file"]["type"] == "image/pjpeg")
		|| ($_FILES["file"]["type"] == "image/x-png")
		|| ($_FILES["file"]["type"] == "image/png"))
		&& ($_FILES["file"]["size"] < 200000)
		&& in_array($extension, $allowedExts)) {
			if ($_FILES["file"]["error"] > 0) {
				$upload_re['return_code'] = "Return Code: " . $_FILES["file"]["error"] . "<br>";
			} else {
				$filename = $label.$_FILES["file"]["name"];
				$upload_re['return_file_name'] =  "Upload: " . $_FILES["file"]["name"] . "<br>";
				$upload_re['return_file_type'] =  "Type: " . $_FILES["file"]["type"] . "<br>";
				$upload_re['return_file_size'] =  "Size: " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
				$upload_re['return_temp_file'] =  "Temp file: " . $_FILES["file"]["tmp_name"] . "<br>";

				if (file_exists("../uploads/" . $filename)) {
					$upload_re['return_already_exist'] =  $filename . " already exists. ";
				} else {
					move_uploaded_file($_FILES["file"]["tmp_name"],
					"../uploads/" . $filename);
					$upload_re['return_stor_id'] =  "Stored in: " . "uploads folder" . $filename;
					$upload_re['return_img'] =  '<img src="/corptest/uploads/' . $filename.'" width="90px" />';
					$upload_re['return_filename'] =  $filename;
				}
			}
		} else {
			$upload_re['return_stat'] =  "Invalid file";
		}

		echo json_encode($upload_re);
	
	}else{
	   switch($fileError){
		 case UPLOAD_ERR_INI_SIZE:   
			  $message = 'Error al intentar subir un archivo que excede el tamaño permitido.';
			  break;
		 case UPLOAD_ERR_FORM_SIZE:  
			  $message = 'Error al intentar subir un archivo que excede el tamaño permitido.';
			  break;
		 case UPLOAD_ERR_PARTIAL:    
			  $message = 'Error: no terminó la acción de subir el archivo.';
			  break;
		 case UPLOAD_ERR_NO_FILE:    
			  $message = 'Error: ningún archivo fue subido.';
			  break;
		 case UPLOAD_ERR_NO_TMP_DIR: 
			  $message = 'Error: servidor no configurado para carga de archivos.';
			  break;
		 case UPLOAD_ERR_CANT_WRITE: 
			  $message= 'Error: posible falla al grabar el archivo.';
			  break;
		 case  UPLOAD_ERR_EXTENSION: 
			  $message = 'Error: carga de archivo no completada.';
			  break;
		 default: $message = 'Error: carga de archivo no completada.';
				  break;
		}
		  echo json_encode(array(
				   'error' => true,
				   'message' => $message
				));
	}
	
}


?>
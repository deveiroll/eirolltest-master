 $(document).ready(function() {
	
	//load the listing
	$( window ).load(function() {
		userListing();
	});

	var userListing = function(){
		//load all list of the users
		
		$('#datatable').dataTable( {
			dom: 	"<'row'<'col-sm-6'l><'col-sm-6'f>>" +
					"<'row'<'col-sm-12'tr>>" +
					"<'row'<'col-sm-5'i><'col-sm-7'p>>"+
					"<'row'<'col-sm-12'B>>",
			"bFilter" : true,               
			"bLengthChange": true,
			buttons: [
				{ 
					extend: 'excel', text: 'Export this list',
					filename: 'Users'
				}
				 
			],
			"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"processing": true,
			
		//	"serverSide": true,
			"ajax": {
				"url": "/corptest/php/action.php?action=listing",
				"type": "POST"
			},
			"columns": [
				{ "data": "id",
					"render": function ( data, type, full, meta ) {
						 var buttonID = full.id;
						 return buttonID+' <span style="float:right;"><a id='+'"pen'+buttonID+'"'+' data="'+buttonID+'" data-toggle="modal" data-target="#modalEditUser" class="user" onclick="doedit(this.id,'+buttonID+')" href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp;&nbsp;<a class="delete" onclick="dodelete('+buttonID+', this.id)" id='+'"trash'+buttonID+'"'+' href="#"><i class="fa fa-trash" aria-hidden="true"></i></a>&nbsp;&nbsp;</span>';
					 }
				},
				{ "data": "photo" },
				{ "data": "firstname" },
				{ "data": "lastname" },
				{ "data": "login" },
				{ "data": "role" },
			]
				
		} );		
	}
	
	
	
	//upload photo
	$("#file").bind("change", function() {

	
		var formData = new FormData();
		formData.append('file', $('#file')[0].files[0]);

		$.ajax({
			  url: "/corptest/php/action.php?action=upload",
			   type : 'POST',
			   data : formData,
			   processData: false,  // tell jQuery not to process the data
			   contentType: false,  // tell jQuery not to set contentType
			   success : function(data) {
				   var filesdata = JSON.parse(data);
				   console.log(data);
				   $('#img_thumb').html(filesdata.return_img);
				   $('#photo').val(filesdata.return_filename);
				   //alert(filesdata.return_filename);
				 //  alert(data);
			   }
		});
	});
	
	
	//create new user
	$("#create_user").on("submit", function(event) {
		event.preventDefault();

		var table = $('#datatable').DataTable();
		
		$.ajax({
			url: "/corptest/php/action.php?action=create",
			type: "post",
			data: $("#create_user").serialize(),
				success: function(d) {
					console.log(d);
					table.ajax.reload();
					$('#modalAddUser').modal('hide');
				}
			});
	});
	
	//update user
	$("#update_user").on("submit", function(event) {
	event.preventDefault();
	var table = $('#datatable').DataTable();
	$.ajax({
		url: "/corptest/php/action.php?action=update&id=",
		type: "post",
		data: $("#update_user").serialize(),
			success: function(d) {
				if (d=='success'){
				table.ajax.reload();
				$('#modalEditUser').modal('hide');
				}
			}
		});
	});
	
 });
 
//this is for delete a user 
function dodelete(delid, sid){
	var table = $('#datatable').DataTable();
	$.ajax({
		url: "/corptest/php/action.php?action=delete",
		type: "post",
		data: { 'selectedid' : delid },
			success: function(d) {
				
				console.log(d);
				
				if(d.status == 'false'){
					
				}else{
					if ( $('#'+sid).hasClass('selected') ) {
						$('#'+sid).removeClass('selected');
					} else {
						table.$('tr.selected').removeClass('selected');
						$('#'+sid).addClass('selected');
					}
					
					table.ajax.reload();
				}
			}
		});
}	

//this is for edit a user 
function doedit(thisid, user_id)
{
	
	//console.log(user_id);
	$.ajax({
		type:"POST",
		datatype: 'json',
		url:'/corptest/php/action.php?edituser='+user_id,
		data: $("#update_user").serialize(),
		success: function(data){	
			var infodata = JSON.parse(data);
			console.log(infodata);
			$('#update_user div.form-group #userid').val(infodata[0].id);
			$('#update_user div.form-group #firstname').val(infodata[0].firstname);
			$('#update_user div.form-group #lastname').val(infodata[0].lastname);
			
			$('#update_user div.form-group #role').val(infodata[0].role);
			
			$('#update_user div.form-group #login').val(infodata[0].login);
			$('#update_user div.form-group #email').val(infodata[0].email);

			$('#update_user div.form-group #password').val(infodata[0].password);
		}
	});
	
	
	
}

	